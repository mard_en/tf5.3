provider "google" {
  project     = "alert-diode-318918"
  region      = "us-central1"
  credentials = "cred.json"
  zone        = "us-central1-a"
}
terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "2.22.0"
    }
    docker = {
      source = "kreuzwerker/docker"
      version = "2.13.0"
    }
  }
}
provider "cloudflare" {
  email      = var.email
  api_key    = var.api_key
  account_id = var.account_id
}
#  provider "docker" {
#    host = "unix://localhost/var/run/docker.sock"
#  }
variable "email" {
  description = "email variable from tfstate file"
}
variable "api_key" {
  description = "api key variable from tfstate file"
}
variable "account_id" {
  description = "account id variable from tfstate file"
}